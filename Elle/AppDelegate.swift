//
//  AppDelegate.swift
//  Elle
//
//  Created by Gabriele Fioretti on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import UserNotifications
import os.log
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    //MARK: - Soluzione: Usare una classe comune sia a watch che iOS
    let sharedInfo = SharedInfo.sharedInfo
    
    var window: UIWindow?
    var userData: UserDefaults
    var user: UserApi
    var helpRequestManager: HelpRequestManager
    var session: WCSession!
    var currentStatus: RequestStatus = .finished
    var lastKnownWatchStatus: RequestStatus? = nil
    
    override init() {
        self.userData = sharedInfo.userData
        self.user = sharedInfo.user
        self.helpRequestManager = sharedInfo.helpRequestManager
        if !helpRequestManager.isActive {
            currentStatus = .finished
        } else {
            currentStatus = .started
        }
    }
    
    public static var API_URL = "https://api.forelle.app/"
    
    public static var App: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    var tokenDevice: String {
        get {
            return UserDefaults.standard.string(forKey: "tokenDevice") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "tokenDevice")
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if helpRequestManager.isActive {
            helpRequestManager.endRequest { success in
                
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Try to configure watchSession
        configureWatchSession()
        // Override point for customization after application launch.
        registerForPushNotifications()
        print("Token: ", self.tokenDevice)
        self.user.loadUser(callback: { (success, error) in
            print("logged \(self.user.isLogged)")
            if self.user.isLogged {
                self.helpRequestManager.loadCurrentRequest()
            }
            self.updateApplicationContext()
        })
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            let notificationOption = launchOptions?[.remoteNotification]
            
            if let notification = notificationOption as? [String: AnyObject], let _ = notification["aps"] as? [String: AnyObject] {
                broadcastNotify(userInfo: notification, showDetail: true)
            }
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        return true
    }
    func application( _ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        self.tokenDevice = token
        if self.tokenDevice.count > 0 && self.user.isLogged {
            self.user.updateTokenDevice(tokenDevice: self.tokenDevice,callback: { (success, error) in
                if success {
                    print("Token device aggiornato")
                    self.updateApplicationContext()
                } else {
                    print("Errore durante l'update del token")
                }
            })
        }
        print("Device Token: \(token)")
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func registerForPushNotifications() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge, .criticalAlert]) { granted, error in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("application didReceiveRemoteNotification")
        guard let _ = userInfo["aps"] as? [String: AnyObject] else {
            completionHandler(.failed)
            return
        }
        broadcastNotify(userInfo: userInfo, showDetail: true)
        //print(userInfo)
        //print(aps)
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground")
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
    }
    func broadcastNotify(userInfo: [AnyHashable: Any], showDetail: Bool) {
        print("broadcastNotify")
        print(userInfo)
        var category = userInfo["category"] as? String
        let HelpRequest = userInfo["HelpRequest"] as? Int
        let requestApi = HelpRequestApi()
        category = category ?? ""
        
        if HelpRequest != nil {
            
            requestApi.find(id: HelpRequest! ) { (success, error, requestData) in
                if(success) {
                    if category == HelpRequestCategory.HelpRequest.rawValue {
                        let name = Notification.Name("notifyHelpRequest")
                        NotificationCenter.default.post(name: name, object: requestData!)
                        if showDetail {
                            DispatchQueue.main.async {
                                let viewTmp = UIStoryboard(name: "Requests", bundle: nil).instantiateViewController(withIdentifier: "requestDetailView") as! RequestDetailViewController
                                viewTmp.helpRequest = requestData
                                if let root = self.window?.rootViewController {
                                    root.present(viewTmp, animated: true)
                                }
                            }
                        }
                    } else if category == HelpRequestCategory.UpdateRequest.rawValue {
                        //Cambio di logica nel backend: non dovrebbe essere più chiamata
                        let name = Notification.Name("notifyUpdateRequest")
                        NotificationCenter.default.post(name: name, object: requestData!)
                        
                    } else if category == HelpRequestCategory.EndRequest.rawValue {
                        let name = Notification.Name("notifyEndRequest")
                        NotificationCenter.default.post(name: name, object: requestData!)
                        if showDetail {
                            DispatchQueue.main.async {
                                let viewTmp = UIStoryboard(name: "Requests", bundle: nil).instantiateViewController(withIdentifier: "requestDetailView") as! RequestDetailViewController
                                viewTmp.helpRequest = requestData
                                if let root = self.window?.rootViewController {
                                    root.present(viewTmp, animated: true)
                                }
                            }
                        }
                    }
                } else {
                    print(error)
                }
            }
        }
    }
    //This is the two delegate method to get the notification in iOS 10..
    //First for foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options:UNNotificationPresentationOptions) -> Void)
    {
        print("Handle push from foreground")
        // custom code to handle push while app is in the foreground
        print("\(notification.request.content.userInfo)")
        guard let _ = notification.request.content.userInfo["aps"] as? [String: AnyObject] else {
            completionHandler(.sound)
            return
        }
        broadcastNotify(userInfo: notification.request.content.userInfo, showDetail: true)
        
    }
    //Second for background and close
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response:UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("Handle push from background or closed")
        print("\(response.notification.request.content.userInfo)")
        
        guard let _ = response.notification.request.content.userInfo["aps"] as? [String: AnyObject] else {
            completionHandler()
            return
        }
        broadcastNotify(userInfo: response.notification.request.content.userInfo, showDetail: true)
        
        completionHandler()
    }
    
}

extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!
    
    /// Logs the view cycles like viewDidLoad.
    static let viewCycle = OSLog(subsystem: subsystem, category: "viewCycleElle")
}

extension AppDelegate: WCSessionDelegate {
    
    func configureWatchSession() {
        if WCSession.isSupported() {
            session = WCSession.default
            session?.delegate = self
            session?.activate()
        }
    }
    
    /**
     * This method sends user data to watch.
     */
    func updateApplicationContext(){
        if let validSession = session{
            let encoder = JSONEncoder()
            do {
                let userData = try encoder.encode(AppDelegate.App.user)
                let userDataStr = String(data: userData, encoding: .utf8)
                let iPhoneAppContext = ["currentUser": userDataStr]
                try validSession.updateApplicationContext(iPhoneAppContext as! [String : String])
                print("Success updating context")
            } catch {
                print("Something went wrong in updating context for session")
            }
        }
    }
    
    func sendStatusToWatch(status: RequestStatus) {
        if let validSession = session {
            do {
                try validSession.updateApplicationContext(["status": status])
            } catch {
                print("Error in updating context!")
            }
        }
    }
    
    func askStatusToWatch() {
        if let validSession = session {
            do {
                try validSession.updateApplicationContext(["requestStatus": true])
            } catch let error as NSError {
                print(error.userInfo)
            }
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        guard error == nil else {
            print(error!.localizedDescription)
            sendStatusToWatch(status: currentStatus)
            askStatusToWatch()
            return
        }
        print("Activation completed with status:")
        switch activationState {
        case .activated:
            print("Activated")
            DispatchQueue.main.async {
                self.updateApplicationContext()
            }
            break
        case .inactive:
            print("Inactive")
            break
        case .notActivated:
            print("Not activated")
            break
        default:
            print("Unknown")
        }
    }
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        print("Recieved info")
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        print("Recieved application context")
        if let status = applicationContext["watchStatus"] as? RequestStatus {
            self.lastKnownWatchStatus = status
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("\n\nRecieved Message\n\n")
        if let recievedState = message["askState"]{
            if (recievedState as! Bool) == true {
                //Il watch vuole avviare una richiesta. Dobbiamo fargli sapere se può(true) o se ne sta facendo una il watch(false)
                if currentStatus == .finished {
                    replyHandler(["state": true])
                } else {
                    replyHandler(["state": false])
                }
            }
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //Try to reactivate
        if WCSession.isSupported() {
            session.activate()
        }
    }
}
