//
//  AudioPlayerViewController.swift
//  Elle
//
//  Created by Andrea Garau on 01/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import AVFoundation

fileprivate enum PlayState {
    case playing
    case pause
}

class AudioPlayerViewController: UIViewController {
    
    public var audioFragment: AudioFragment? {
        didSet {
            if self.audioFragment != nil {
                self.downloadAudioData()
                self.updateUI()
            }
        }
    }
    
    private var audioData: Data?
    private var playState: PlayState = .pause
    
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerSlider: UISlider!
    @IBOutlet weak var audioTitle: UILabel!
    @IBOutlet weak var audioDurationLabel: UILabel!
    
    @IBOutlet weak var downloadView: UIView!
    
    private var audioPlayer: AVAudioPlayer!
    
    private var audioTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Close button
        let dismissGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissAudioPlayer(_:)))
        closeView.addGestureRecognizer(dismissGestureRecognizer)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error as NSError {
            print(error)
        }
        
        self.playerSlider.addTarget(self, action: #selector(onSliderValueChanged), for: .valueChanged)
        
    }
    
    @objc func onSliderValueChanged() {
        self.audioPlayer.currentTime = TimeInterval(playerSlider.value)
    }

    @objc func dismissAudioPlayer(_ sender: UITapGestureRecognizer) {
        self.audioPlayer.stop()
        self.audioTimer?.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
    
    private func downloadAudioData() {
        let downloadTask = URLSession.shared.dataTask(with: audioFragment!.audioURL) { (data, _, error) in
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                    return
                }
            }
            if let data = data {
                DispatchQueue.main.async {
                    self.downloadView.isHidden = true
                    self.audioData = data
                    do {
                        self.audioPlayer = try AVAudioPlayer(data: self.audioData!)
                        self.audioPlayer.delegate = self
                        self.audioPlayer.prepareToPlay()
                        self.playerSlider.maximumValue = Float(self.audioPlayer.duration)
                        self.audioDurationLabel.text = self.stringFromTimeInterval(interval: self.audioPlayer.duration)
                    } catch {
                        print("Cannot initialize AVAudioPlayer")
                    }
                    
                }
                return
            }
        }
        downloadTask.resume()
    }
    
    private func updateUI() {
        if let currentAudioFragment = audioFragment {
            DispatchQueue.main.async {
                self.audioTitle.text = currentAudioFragment.creationDate != nil ? Utils.dateToString(date: currentAudioFragment.creationDate!) : NSLocalizedString("NO_INFO", comment: "Nessuna informazione disponibile")
            }
        }
    }
    
    @objc func updateTime(_ timer: Timer) {
        DispatchQueue.main.async {
            self.playerSlider.value = Float(self.audioPlayer.currentTime)
        }
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    private func updatePlayButton(with state: PlayState) {
        switch state {
        case .pause:
            self.playState = state
            self.playButton.setImage(UIImage(systemName: "play.fill"), for: .normal)
        case .playing:
            self.playState = state
            self.playButton.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        }
    }

    //MARK: - IBActions
    
    @IBAction func playButtonPressed(_ sender: Any) {
        guard let audioPlayer = audioPlayer else {
            return
        }
        
        if playState == .pause {
            self.audioTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime(_:)), userInfo: nil, repeats: true)
            self.playerSlider.value = 0.0
            audioPlayer.play()
            self.updatePlayButton(with: .playing)
        } else {
            audioPlayer.pause()
            self.updatePlayButton(with: .pause)
        }
        
    }
}

extension AudioPlayerViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.updatePlayButton(with: .pause)
    }
}

