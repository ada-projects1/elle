//
//  HalfSizePresentationController.swift
//  Elle
//
//  Created by Andrea Garau on 01/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit

class HalfSizePresentationController: UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else {
                return CGRect.zero
            }
            let height: CGFloat = 300.0
            return CGRect(x: 0, y: theView.bounds.height - height, width: theView.bounds.width, height: height)
        }
    }
}
