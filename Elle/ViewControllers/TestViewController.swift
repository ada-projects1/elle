//
//  TestViewController.swift
//  Elle
//
//  Created by Fabio Palladino on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {
    @IBOutlet weak var textLog: UITextView!
    
    let app = AppDelegate.App
    var helpRequest: HelpRequestDataResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onClickFriends(_ sender: UIButton) {
        if !app.user.isLogged {
            Utils.ShowMessage(vc: self, title: "Test", message: "Non sei loggato")
            return
        }
        textLog.text = ""
        app.user.loadFriends { (success, error) in
            DispatchQueue.main.async {
                if success {
                    self.textLog.text = "Caricati \(self.app.user.friends.friends.count) Friends"
                } else {
                    self.textLog.text = error
                }
            }
            
        }
    }
    @IBAction func onClickFindFriends(_ sender: Any) {
        if !app.user.isLogged {
            Utils.ShowMessage(vc: self, title: "Test", message: "Non sei loggato")
            return
        }
        textLog.text = ""
        let friends: [String] = ["12345","123456789"]
        
        app.user.findFriends(friends: friends, callback: { (success, error, friendsFound) in
            DispatchQueue.main.async {
                if success {
                    if let friendsFound = friendsFound {
                        self.textLog.text = "Trovati \(friendsFound.count) Friends"
                    }
                    
                } else {
                    self.textLog.text = error
                }
            }
        })
    }
    @IBAction func onClickListRequest(_ sender: UIButton) {
        let helpRequest = HelpRequestApi()
        helpRequest.search { (success, error, datiRequest) in
            DispatchQueue.main.async {
                if success {
                    if let datiRequest = datiRequest {
                        print(datiRequest)
                        self.textLog.text = "Requests Trovate \(datiRequest.count)"
                    }
                    
                } else {
                    self.textLog.text = error
                }
            }
        }
    }
    @IBAction func onClickAddRequest(_ sender: Any) {
        if helpRequest != nil {
            Utils.ShowMessage(vc: self, title: "Attention", message: "Request Active")
            return
        }
        let requestApi = HelpRequestApi()
        var hr =  HelpRequestAdd()
        hr.description = "TEST"
        hr.lat = 24.00000
        hr.lon = 45.00000
        hr.serverity = 1
    
        requestApi.addRequest(helpRequest: hr) { (success, error, datiRequest) in
            DispatchQueue.main.async {
                if success {
                    if let datiRequest = datiRequest {
                        print(datiRequest)
                        self.helpRequest = datiRequest
                        self.textLog.text = "Requests Id \(datiRequest.helpRequest.id)"
                    }
                    
                } else {
                    self.textLog.text = error
                }
            }
        }
    }
    
    @IBAction func onClickStopRequest(_ sender: Any) {
        let requestApi = HelpRequestApi()
        if let helpRequest = self.helpRequest?.helpRequest {
            requestApi.updateRequest(helpRequestId: helpRequest.id, severity: 0, active: 0) { (success, error, datiRequest) in
                if success {
                    self.helpRequest = nil
                    if let datiRequest = datiRequest {
                        print(datiRequest)
                        self.textLog.text = "Requests Id \(datiRequest.helpRequest.id) Stop"
                    }
                } else {
                    self.textLog.text = error
                }
            }
        } else {
            Utils.ShowMessage(vc: self, title: "Attention", message: "Request Not Active")
        }
        
    }
    @IBAction func onClickAddDetail(_ sender: UIButton) {
        let requestApi = HelpRequestApi()
        if let helpRequest = self.helpRequest?.helpRequest {
            let detail = HelpRequestDetailAdd(helpRequestId: helpRequest.id, lat: 15.0, lon: 45.0, audioFileBase64: "")
            
            requestApi.addRequestDetail(detail: detail, callback: { (success, error, datiDetail) in
                if success {
                   if let datiDetail = datiDetail {
                        print(datiDetail)
                        self.textLog.text = "DETAIL Id \(datiDetail.id)"
                    }
                } else {
                    self.textLog.text = error
                }
            })
        } else {
            Utils.ShowMessage(vc: self, title: "Attention", message: "Request Not Active")
        }
    }
    @IBAction func onClickFindRequest(_ sender: UIButton) {
        let requestApi = HelpRequestApi()
        if let helpRequest = self.helpRequest?.helpRequest {
            
            requestApi.find(id: helpRequest.id) { (success, error, datiRequest) in
                if success {
                    if let datiRequest = datiRequest {
                        print(datiRequest)
                        self.textLog.text = "Request Id \(datiRequest.helpRequest.id)"
                    }
                } else {
                    self.textLog.text = error
                }
            }
        } else {
            Utils.ShowMessage(vc: self, title: "Attention", message: "Request Not Active")
        }
    }
    @IBAction func onClickActiveRequest(_ sender: UIButton) {
        let helpRequest = HelpRequestApi()
        helpRequest.activeRequests { (success, error, datiRequest) in
            DispatchQueue.main.async {
                if success {
                    if let datiRequest = datiRequest {
                        print(datiRequest)
                        self.textLog.text = "Requests Trovate \(datiRequest.count)"
                    }
                    
                } else {
                    self.textLog.text = error
                }
            }
        }
    }
    @IBAction func onClickCurrentRequest(_ sender: UIButton) {
        let helpRequest = HelpRequestApi()
        helpRequest.currentRequests { (success, error, datiRequest) in
            DispatchQueue.main.async {
                if success {
                    if let datiRequest = datiRequest {
                        print(datiRequest)
                        self.textLog.text = "Requests Trovate \(datiRequest.count)"
                    }
                    
                } else {
                    self.textLog.text = error
                }
            }
        }
    }
    
}
