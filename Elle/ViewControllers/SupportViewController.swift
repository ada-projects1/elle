//
//  SupportViewController.swift
//  Elle
//
//  Created by Giusy Di Paola on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
import WatchConnectivity

class SupportViewController: UIViewController {
    
    @IBOutlet weak var questionMarkIcon: UIBarButtonItem!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var infoTableView: UITableView!
    
    
    //Variables
    var date: Date!
    var locationManager = CLLocationManager()
    var labelExtension = UILabel()
    var pulse: PulseAnimation?
    var pulse1: PulseAnimation?
    
    //infoView Variables
    var helpRequestInProgress = [HelpRequestDataResponse]()
    var lastSeenUser = [LastSeenFriend]()
    var inProgressTimer: Timer?
    
    
    //MARK: - Localized strings
    let notRecordingStr = NSLocalizedString("INFOLABEL_NOT_RECORDING", comment: "Not recording state is: \"Tap to get help\"")
    let recordingStr = NSLocalizedString("INFOLABEL_RECORDING", comment: "Recording state is: \"Sending your request\"")
    let noFriendsTitle = NSLocalizedString("ALERT_NO_FRIENDS_TITLE", comment: "Title of alert that appears if you try to start a req with no friends")
    let noFriendsMsg = NSLocalizedString("ALERT_NO_FRIENDS_MESSAGE", comment: "Message of alert that appears if you try to start a req with no friends")
    let startReqTitle = NSLocalizedString("ALERT_START_REC_TITLE", comment: "Title of the alert that appears when you start sending a request")
    let settingsActionTitle = NSLocalizedString("ALERT_SETTINGS_ACTION_TITLE", comment: "Go to settings of app")
    let settingsAlertTitle = NSLocalizedString("ALERT_SETTINGS_TITLE", comment: "Alert to go to settings")
    let settingsAlertMsg = NSLocalizedString("ALERT_SETTINGS_MESSAGE", comment: "Go to settings")
    let cancelActionMsg = NSLocalizedString("ALERT_CANCEL_ACTION", comment: "Cancel action")
    let audioErrAlertTitle = NSLocalizedString("ALERT_ADUIOERROR_TITLE", comment: "Error with audio rec")
    let audioErrMsg = NSLocalizedString("ALERT_AUDIOERROR_MESSAGE", comment: "Error in audio")
    let okActionStr = NSLocalizedString("ALERT_OK_ACTION", comment: "Ok")
    
    let helpRequestManager = AppDelegate.App.helpRequestManager
    let delegate = AppDelegate.App
    
    //Settings to audio recorder. To save in m4a in high quality
    let recordSettings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 12000,
        AVNumberOfChannelsKey: 1,
        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoView.isHidden = true
        
        // Fade in - Fade out -> Label
        labelExtension.fadeViewInThenOut(view: infoLabel, delay: 0.0)
        
        // The stop button is hidden
        stopButton.isHidden = true
        self.infoLabel.text = notRecordingStr
        
        //Request location permissions
         self.helpRequestManager.locationManager.requestWhenInUseAuthorization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifyHelpRequest), name: Notification.Name("notifyHelpRequest"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifyUpdateRequest), name: Notification.Name("notifyUpdateRequest"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onNotifyEndRequest), name: Notification.Name("notifyEndRequest"), object: nil)
        //To resume animation from background
        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        //Info View
        infoTableView.dataSource = self
        infoTableView.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        createAnimationButton()
        showHideInfo()
        if helpRequestManager.isActive {
            recordingAnimations()
        } else {
            stopAnimations()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        stopInfoView()
    }

    @objc func willEnterForeground() {
        self.viewDidAppear(true)
    }
    
    @objc func onNotifyHelpRequest(notification: Notification) {
        showHideInfo()
    }
    //Notifica chiamata all'arrivo di un nuovo messaggio audio
    @objc func onNotifyUpdateRequest(notification: Notification) {
        showHideInfo()
    }
    //Notifica chiamata alla fine di una richiesta
    @objc func onNotifyEndRequest(notification: Notification) {
        showHideInfo()
    }
    func displayNewRequestPendingAlert(title: String) -> UIAlertController {
        //create an alert controller
        let pending = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        
        //Setting view size to half of screen
        let height: NSLayoutConstraint = NSLayoutConstraint(item: pending.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.2)
        pending.view.addConstraint(height);

        //create an activity indicator
        let indicator = UIActivityIndicatorView(frame: pending.view.bounds)
        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        //add the activity indicator as a subview of the alert controller's view
        pending.view.addSubview(indicator)
        indicator.isUserInteractionEnabled = false // required otherwise if there buttons in the UIAlertController you will not be able to press them
        indicator.startAnimating()

        self.present(pending, animated: true, completion: nil)
        return pending
    }
    
    func createAnimationButton() {
        if self.pulse != nil {
            self.mainButton.layer.sublayers?.removeLast()
            self.mainButton.layer.sublayers?.removeLast()
            self.mainButton.layer.removeAllAnimations()
        }
        self.pulse = PulseAnimation(numberOfPulse: Float.infinity, radius: 140, postion: .zero)
        self.pulse1 = PulseAnimation(numberOfPulse: Float.infinity, radius: 190, postion: .zero)
        
        //Adding infolabel
        self.infoLabel.isHidden = false
        self.infoLabel.text = notRecordingStr
        self.infoLabel.alpha = 1
        // Fade in - Fade out -> Label
        fadeViewInThenOut(view: infoLabel, delay: 0.0)
        
        // Animation Pulse --> Main Button
        pulse?.position = CGPoint(x: mainButton.frame.size.width/2.0, y: mainButton.frame.size.width/2.0)
        pulse?.animationDuration = 2.0
        pulse?.isHidden = true
        pulse?.backgroundColor = #colorLiteral(red: 0.876861155, green: 0.6074052453, blue: 0.7626455426, alpha: 1)
        self.mainButton.layer.addSublayer(pulse!)
        pulse1?.position = CGPoint(x: mainButton.frame.size.width/2.0, y: mainButton.frame.size.width/2.0)
        pulse1?.animationDuration = 3.0
        pulse1?.isHidden = true
        pulse1?.backgroundColor = #colorLiteral(red: 0.2455809116, green: 0.3030403852, blue: 0.2989798188, alpha: 1)
        self.mainButton.layer.addSublayer(pulse1!)
        // End ---> Animation Pulse --->Main Button
    }
    
    // This is the main button which will perform all the actions
    @IBAction func sendRequest(_ sender: UIButton) {
        //E if !delegate.session.isReachable  ???
        if delegate.session.isReachable { //Chech if watch is reachable
            let alert = displayNewRequestPendingAlert(title: "Checking watch state")
            AppDelegate.App.session.sendMessage(["askState": true], replyHandler: { (reply) in
                DispatchQueue.main.async {
                    alert.dismiss(animated: true, completion: nil)
                }
                if let recievedState = reply["state"] as? Bool {
                    print("\n\nReply recieved: State = \(recievedState); \(recievedState ? "watch isn't active" : "watch is active")\n\n")
                    if !recievedState {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Watch is active", message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                        }
                    } else {
                        self.startRecording()
                    }
                }
            }) { (error) in
                print(error.localizedDescription)
                //MARK: Scegliamo di far fare lo stesso la richiesta ?
            }
        } else {
            self.startRecording()
        }
    }
    
    /**
     * - Description: Starts recording sending server request
     * - Parameters:
     *  - Nothing: no parameter
     */
    func startRecording() {
        
        if !AppDelegate.App.user.isLogged {
            performSegue(withIdentifier: "toLoginSegue", sender: self)
            return
        }
        
        if AppDelegate.App.user.friends.friends.isEmpty {
            let alert = UIAlertController(title: noFriendsTitle, message: noFriendsMsg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true)
            return
        }

        do {
            //Initialize AVSession to record
            helpRequestManager.avSession = AVAudioSession.sharedInstance()
            try helpRequestManager.avSession.setCategory(AVAudioSession.Category.playAndRecord)
            try helpRequestManager.avSession.setActive(true)
            
            //Initialize location manager (Check permissions)
            helpRequestManager.initializeLocationManager()
            
            //Create permission alert
            let settingsAction = UIAlertAction(title: settingsActionTitle, style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }
            let permitAlert = UIAlertController(title: settingsAlertTitle, message: settingsAlertMsg, preferredStyle: UIAlertController.Style.alert)
            permitAlert.addAction(UIAlertAction(title: cancelActionMsg, style: .cancel) {action in
                return
            })
            permitAlert.addAction(settingsAction)
            
            //Check permissions
            helpRequestManager.avSession.requestRecordPermission() { granted in DispatchQueue.main.async {
                if !granted {
                    self.present(permitAlert, animated: true, completion: nil)
                } else {
                    self.helpRequestManager.loadCurrentRequest()
                    if !self.helpRequestManager.isActive { //Nuova Help request
                        let alert = self.displayNewRequestPendingAlert(title: self.startReqTitle)
                        self.delegate.currentStatus = .starting
                        self.helpRequestManager.startRecording { success in
                            if success {
                                DispatchQueue.main.async {
                                    alert.dismiss(animated: true, completion: nil)
                                    self.delegate.currentStatus = .started
                                    self.recordingAnimations()
                                    self.helpRequestManager.date = Date()
                                }
                            }
                        }
                    } else {//Aumentare severity
                        self.helpRequestManager.updateSeverity { (success, errorMsg, response) in
                            if success {
                                self.updateColorOnSeverity()
                                print("Success")
                            } else {
                                print(errorMsg)
                            }
                        }
                    }
                }
                }
            }
        } catch {
            // failed to record!
            print("Failed to record")
            finishRecording(success: false)
        }
    }
    
    func updateColorOnSeverity() {
        DispatchQueue.main.async {
            switch self.helpRequestManager.helpRequest?.helpRequest.serverity{
            case 1:
                self.pulse?.backgroundColor = #colorLiteral(red: 0.876861155, green: 0.6074052453, blue: 0.7626455426, alpha: 1)
                break
            
            case 2:
                self.pulse?.backgroundColor = #colorLiteral(red: 0.4726639986, green: 0.761321485, blue: 0.7046354413, alpha: 1)
                self.infoLabel.text = "Severity level: Medium"
                self.infoLabel.text = NSLocalizedString("Medium", comment: "Media")
                break
            case .none:
                self.pulse?.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            case .some(let n):
                if n > 2 {
                    self.pulse?.backgroundColor = #colorLiteral(red: 0.4593612552, green: 0.2311561108, blue: 0.4601395726, alpha: 1)
                    self.infoLabel.text = "Severity level: High"
                    self.infoLabel.text = NSLocalizedString("High", comment: "Alta")
                }
            }
        }
    }
    
    func finishRecording(success: Bool) {
        if (!success) {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: self.audioErrAlertTitle, message: self.audioErrMsg, preferredStyle: .alert)
                let okAction = UIAlertAction(title: self.okActionStr, style: .default) { (action) in
                    alert.dismiss(animated: true, completion: nil)
                }
                alert.addAction(okAction)
                self.present(alert, animated: true)
            }
            self.helpRequestManager.finishRecording(success: false, isLastUpdate: false) { success in
                
            }
        } else {
            self.helpRequestManager.finishRecording(success: true, isLastUpdate: false) { success in
                           
            }
        }
    }
    
    //MARK: - Button animations--------------------------------------------
    func recordingAnimations(){
        updateColorOnSeverity()
        self.infoLabel.isHidden = false
        self.infoLabel.text = recordingStr
        self.pulse?.isHidden = false
        self.pulse1?.isHidden = false
        self.stopButton.isHidden = false
        showHideInfo()
    }
    
    func stopAnimations() {
        self.infoLabel.isHidden = false
        stopButton.isHidden = true
        self.pulse?.isHidden = true
        self.pulse1?.isHidden = true
        infoLabel.text = notRecordingStr
        showHideInfo()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //MARK: TODO
        
    }
    
    func settingsButtonPressed(sender:UIButton) {
        let storyboard = UIStoryboard(name: "AccountLinking", bundle: nil)
        let linkingVC = storyboard.instantiateViewController(withIdentifier: "AccountLinkingTable")
        self.navigationController?.pushViewController(linkingVC, animated: true)
    }
    
    @IBAction func stopButtonIsPressed(_ sender: UIButton) {
        let alert = self.displayNewRequestPendingAlert(title: self.startReqTitle)
        self.delegate.currentStatus = .finishing
        self.helpRequestManager.endRequest { success in
            DispatchQueue.main.async {
                self.stopAnimations()
                self.delegate.currentStatus = .finished
                self.pulse?.isHidden = true
                self.pulse1?.isHidden = true
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // Animation fade in - fade out [LABELS]
    func fadeViewInThenOut(view : UILabel, delay: TimeInterval) {
        let animationDuration = 2.3
        UIView.animate(withDuration: animationDuration, delay: delay, options: [UIView.AnimationOptions.autoreverse, UIView.AnimationOptions.repeat], animations: {
            view.alpha = 0
        }, completion: nil)
    }
    
    @IBAction func whenQuestionMarkIconIsPressed(_ sender: UIBarButtonItem) {
        //UserDefaults.standard.set(false, forKey: "tutorialAccepted")
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstVC") as? TutorialViewController
        vc?.userDef = false
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
//MARK: Gestione TableView per mostrare richieste in corso o i forellers che hanno visto la richiesta
extension SupportViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func showHideInfo() {
        
        if self.helpRequestManager.isActive {
            self.infoView.isHidden = false;
            self.infoTitleLabel.text = NSLocalizedString("TITLE_LAST_SEEN", comment: "")
            activeTimerInfoView()
        } else {
            self.infoView.isHidden = true;
            let api = HelpRequestApi()
            api.activeRequests { (success, error, helpRequestActive) in
                if success && helpRequestActive != nil {
                    DispatchQueue.main.async {
                        self.helpRequestInProgress = helpRequestActive!
                        if self.helpRequestInProgress.count > 0 {
                            self.infoView.isHidden = false;
                            self.infoTitleLabel.text = NSLocalizedString("TITLE_REQUEST_INPROGRESS", comment: "")
                            self.activeTimerInfoView()
                        }
                    }
                } else {
                    print("showHideInfo -> ",error)
                }
            }
        }
        
    }
    func activeTimerInfoView() {
        DispatchQueue.main.async {
            if self.infoView.isHidden == false {
                self.infoTableView.reloadData()
                
                if self.inProgressTimer != nil {
                    self.inProgressTimer?.invalidate()
                    self.inProgressTimer = nil
                }
                self.inProgressTimer =  Timer.scheduledTimer(timeInterval: 10,
                                                       target: self,
                                                       selector: #selector(self.timerInProgress),
                                                       userInfo: nil,
                                                       repeats: true)
            }
        }
    }
    func stopInfoView() {
        if inProgressTimer != nil {
            inProgressTimer?.invalidate()
            inProgressTimer = nil
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.helpRequestManager.isActive {
            return lastSeenUser.count
        }
        return helpRequestInProgress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        if self.helpRequestManager.isActive {
            let cell = infoTableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
            let row = lastSeenUser[indexPath.row]
            cell.textLabel?.text = row.name
            cell.detailTextLabel?.text = Utils.formatDate(string: row.dateLastSeen)
            cell.layer.cornerRadius = 18
            cell.layer.masksToBounds = true
            return cell
        }
        
        let cell = infoTableView.dequeueReusableCell(withIdentifier: "cellIDRequest", for: indexPath)
        let row = helpRequestInProgress[indexPath.row]
        cell.textLabel?.text = row.helpRequest.description
        cell.detailTextLabel?.text = Utils.formatDate(string: row.helpRequest.dateInsert)
        cell.layer.cornerRadius = 18
        cell.layer.masksToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if self.helpRequestManager.isActive {
            return
        }
        let requestData = helpRequestInProgress[indexPath.row]
        let viewTmp = UIStoryboard(name: "Requests", bundle: nil).instantiateViewController(withIdentifier: "requestDetailView") as! RequestDetailViewController
        viewTmp.helpRequest = requestData
        self.navigationController?.pushViewController(viewTmp, animated: true)
    }

    @objc func timerInProgress() {
        print("timerInProgress \(Date())")
        if self.helpRequestManager.isActive {
            let api = HelpRequestApi()
            if let  helpRequestId = helpRequestManager.helpRequest?.helpRequest.id {
                api.lastSeenFriends(id: helpRequestId) { (success, error, lastSeenFriends) in
                    if success {
                        if let lastSeenFriends = lastSeenFriends {
                            self.lastSeenUser = lastSeenFriends
                            DispatchQueue.main.async {
                                self.infoTableView.reloadData()
                            }
                        }
                    }
                }
            }
        } else {
            let api = HelpRequestApi()
            api.activeRequests { (success, error, helpRequestActive) in
                if success {
                    if let helpRequestActive = helpRequestActive {
                        self.helpRequestInProgress = helpRequestActive
                        DispatchQueue.main.async {
                            self.infoTableView.reloadData()
                        }
                    }
                }
            }
        }
    }
}




