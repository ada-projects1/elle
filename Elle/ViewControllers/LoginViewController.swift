//
//  LoginViewController.swift
//  Elle
//
//  Created by Giusy Di Paola on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import AuthenticationServices

class LoginViewController: UIViewController,ASAuthorizationControllerDelegate {
    @IBOutlet weak var textLogin: UITextField!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var viewAppleLogin: UIView!
    @IBOutlet weak var loginTitle: UILabel!
    
    let border : UIColor = UIColor(red: 27.0/255.0, green: 29.0/255.0, blue: 34.0/255.0, alpha: 1.0 )
    var authorizationButton: ASAuthorizationAppleIDButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Placeholder
        
        //textName.attributedPlaceholder = "Type Your Name".toAttributed(alignment: .center)
        //textLogin.attributedPlaceholder = "Type Your Mobile Number".toAttributed(alignment: .center)
        
        //MARK: Design TxtField
        textName.layer.cornerRadius = 25.0
        textName.clipsToBounds = true
        textName.layer.borderColor = border.cgColor
        textName.layer.borderWidth = 1.0
        //--------------------------------------------------
        textLogin.layer.cornerRadius = 25.0
        textLogin.clipsToBounds = true
        textLogin.layer.borderColor = border.cgColor
        textLogin.layer.borderWidth = 1.0
        
        //MARK: DESIGN BUTTONS
        
        
        
        buttonLogout.layer.masksToBounds = true
        buttonLogout.layer.borderWidth = 0.2
        buttonLogout.layer.cornerRadius = 25.0
        
        // Do any additional setup after loading the view.
        //Keyboard Handler
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        //view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Design Button
        if authorizationButton == nil {
            setUpSignInAppleButton()
        }
        
        let app = AppDelegate.App
        if app.user.isLogged {
            loginTitle.text = "You are logged"
            loginTitle.text = NSLocalizedString("Registration", comment: "Logged")
            textName.isUserInteractionEnabled = false
            //buttonRegister.isHidden = true
            //buttonLogin.isHidden = true
            viewAppleLogin.isHidden = true
            authorizationButton?.isHidden = true
            buttonLogout.isHidden = false
            textName.text = app.user.user.name
            textLogin.text = app.user.user.number
            textLogin.isEnabled = false
        } else {
            //buttonRegister.isHidden = false
            //buttonLogin.isHidden = false
            viewAppleLogin.isHidden = false
            authorizationButton?.isHidden = false
            buttonLogout.isHidden = true
            textLogin.isEnabled = true
        }
    }
    
    // DISMISS KEYBOARD
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    //-------------------------------------------------------
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    func setUpSignInAppleButton() {
        authorizationButton = ASAuthorizationAppleIDButton(type: .signIn, style: .whiteOutline)
        authorizationButton?.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        
        //authorizationButton.layer.cornerRadius = 25.0
        //authorizationButton?.layer.frame = CGRect(x: 0, y: 0, width: self.viewAppleLogin.bounds.width - self.viewAppleLogin.frame.origin.x, height: self.viewAppleLogin.bounds.height)
        authorizationButton?.layer.frame = self.textName.frame //CGRect(x: 0, y: 0, width: self.viewAppleLogin.bounds.width - self.viewAppleLogin.frame.origin.x, height: self.viewAppleLogin.bounds.height)
        authorizationButton?.layer.masksToBounds = true
        authorizationButton?.layer.borderColor = border.cgColor
        
        authorizationButton?.layer.masksToBounds = true
        authorizationButton?.layer.borderWidth = 0.2
        authorizationButton?.layer.cornerRadius = 25.0
        
        //Add button on some view or stack
        //let view = UIView()
        //view.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
        authorizationButton?.frame = CGRect(x: self.view.safeAreaInsets.left + 35,
                                            y: viewAppleLogin.frame.origin.y,
                                            width:  self.view.frame.width - self.view.safeAreaInsets.left - 35 - self.view.safeAreaInsets.right - 35,
                                            height: self.viewAppleLogin.bounds.height)
        self.view.addSubview(authorizationButton!)
        //self.viewAppleLogin.addSubview(authorizationButton!)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func onClickLogout(_ sender: UIButton) {
        let app = AppDelegate.App
        app.user.logout()
        self.dismiss(animated: true)
    }
    @objc func handleAppleIdRequest() {
        let username = textLogin.text!
        let name = textName.text!
        
        let app = AppDelegate.App
        if name.count == 0 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_NAME", comment: "Name Field"))
            return
        }
        if username.count == 0 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_NUMBER", comment: "Number Field"))
            return
        }
        #if targetEnvironment(simulator)
        app.tokenDevice = "SIMULATOR"
        #else
        if app.tokenDevice.count == 0 {
            Utils.ShowMessage(vc: self, title: NSLocalizedString("LOGIN_TITOLO_ALERT_MANDATORY", comment: ""), message: NSLocalizedString("LOGIN_MANDATORY_DEVICE", comment: "Device Field"))
            return
        }
        #endif
        
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName,.email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
        
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            let userToken = appleIDCredential.user
            
            let name =  textName.text! //(fullName?.givenName ?? "") + " " + (fullName?.familyName ?? "")
            let number  = textLogin.text!
            //            //textPassword.text = userToken
            
            print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
            
            let app = AppDelegate.App
            
            var user = UserCreate()
            user.name = name
            user.password = userToken
            user.number = number
            user.username = userToken
            user.tokenDevice = app.tokenDevice
            
            app.user.createUser(user: user) { (success, error) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true)
                }
            }
        }
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print(error)
        //Utils.ShowMessage(vc: self, title: "Login Error", message: error.localizedDescription)
    }
}
