//
//  AddContactViewController.swift
//  Elle
//
//  Created by Mario Armini on 25/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import Contacts


class AddContactViewController: UIViewController {
    let app = AppDelegate.App
    
    struct prefContact{
        var name: String
        var number: String
    }
    
    struct section {
        var letter : String
        var contact : [UserResponse]
    }

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    var contacts = [UserResponse]()
    var contactsNumbers = [String]()
    var preferredContacts = [prefContact]()
    
    var contactSections = [section]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundView = UIImageView(image: UIImage(named: "Rectangle.png"))
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 0, y: 15, width: tableView.frame.width, height: 30)
        button.setTitle(NSLocalizedString("MESSAGGIO_CONDIVIDI", comment: "Invite friends"), for: .normal)
        button.setTitleColor(UIColor(red: 0.745, green: 0.561, blue: 0.627, alpha: 1), for: .normal)
        button.isUserInteractionEnabled = true
        button.addTarget(self, action: #selector (inviteFriends), for: .touchUpInside)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        view.addSubview(button)
        
        tableView.tableFooterView = view
        tableView.sectionIndexColor = UIColor(red: 0.745, green: 0.561, blue: 0.627, alpha: 1)
        
        if !app.user.isLogged{
            let refreshAlert = UIAlertController(title: NSLocalizedString("TITOLO_ALERT", comment: "Title"), message: NSLocalizedString("MESSAGGIO_ALERT", comment: "Message"), preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                self.navigationController?.popToRootViewController(animated: true)
                
            }))
            present(refreshAlert, animated: true, completion: nil)
           
        }
        fetchContacts()
    }
    
    @objc func inviteFriends(){
        let stringToShare = NSLocalizedString("TESTO_CONDIVISIONE", comment: "Shared text")
        let objectsToShare = [stringToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    private func fetchContacts(){
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) {(granted, error) in
            if let error = error{
                print("Failed to request access", error)
                return
            }
            if granted{
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do{
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        self.contactsNumbers.append(contact.phoneNumbers.first?.value.stringValue ?? "")
                    })
                    self.verifyContacts()
                } catch let error {
                    print("Failed to enumerate contacts", error)
                }
            } else {
                print("Access denied")
            }
        }
    }
    
    ///Verifies if contacts numbers are in the DB
    func verifyContacts(){
        app.user.findFriends(friends: contactsNumbers, callback: { (success, error, friendsFound) in
            DispatchQueue.main.async {
                if success{
                    if let friendsFound = friendsFound{
                        print("\(friendsFound.count) friends founded")
                        self.contacts = friendsFound
                        self.generateContactsSection()
                        self.tableView.reloadData()
                    }
                }else{
                    print(error)
                }
            }
        })
    }
    
    func generateContactsSection(){
        let groupedDictionary = Dictionary(grouping: contacts, by: {String($0.name.first ?? "#")})
        let keys = groupedDictionary.keys.sorted()
        contactSections = keys.map{section(letter: $0, contact: groupedDictionary[$0]!) }
    }
    
    @IBAction func doneAction(_ sender: Any) {
        let index = tableView.indexPathsForSelectedRows
        var indexToPass = [Int]()
        if index != nil {
            for item in index!{
                //indexToPass.append(contacts[item.row].id)
                indexToPass.append(contactSections[item.section].contact[item.row].id)
            }
            print(indexToPass)
        }
        if indexToPass.count == 0 {
            Utils.ShowMessage(vc: self, title: "Attention", message: "Please select at least one contact")
            return
        }
        app.user.addFriend(friendId: indexToPass) { (success, error, user) in
            if success{
                print("Added \(indexToPass.count) friends")
                self.app.user.loadFriends { (success, error) in
                    print(self.app.user.friends.friends)
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
            }
            else{
                print(error)
            }
        }
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AddContactViewController : UITableViewDelegate{
    
}

extension AddContactViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactSections[section].contact.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return contactSections.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell2", for: indexPath) as! FavouritesContactTableViewCell
        let section = contactSections[indexPath.section]
        let contact = section.contact[indexPath.row]
        cell.nameLabel.text = contact.name
        cell.numberLabel.text = contact.number
        let button = UIButton(type: .custom)
        button.setImage(UIImage(systemName: "heart"), for: .normal)
        button.sizeToFit()
        button.tag = indexPath.row
        button.isUserInteractionEnabled = false
        cell.selectionStyle = .none
        cell.accessoryView = button
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return contactSections[section].letter
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return contactSections.map{$0.letter}
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor(red: 0.122, green: 0.129, blue: 0.149, alpha: 1)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor(red: 0.745, green: 0.561, blue: 0.627, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FavouritesContactTableViewCell
        let button = UIButton(type: .custom)
        button.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        button.sizeToFit()
        button.isUserInteractionEnabled = false
        cell.accessoryView = button
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FavouritesContactTableViewCell
        let button = UIButton(type: .custom)
        button.setImage(UIImage(systemName: "heart"), for: .normal)
        button.sizeToFit()
        button.isUserInteractionEnabled = false
        cell.accessoryView = button
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
         return true
     }
}
