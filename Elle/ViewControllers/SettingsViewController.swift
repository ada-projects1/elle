//
//  Settings.swift
//  Elle
//
//  Created by Giusy Di Paola on 25/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    @IBOutlet var tableViewSettings: UITableView!
    
    var arrayTitles : Array = ["Tutorial"]
    var use = TutorialViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        use.userDef = false
        
        self.tableViewSettings.dataSource = self
        self.tableViewSettings.delegate = self
        self.tableViewSettings.reloadData()
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell
        cell?.tutorialLabel.text = arrayTitles[indexPath.row]
        return cell!
    }
  
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // It's not in the same storyboard.
        
        UserDefaults.standard.set(false, forKey: "tutorialAccepted")
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstVC") as? TutorialViewController
         self.navigationController?.pushViewController(vc!, animated: true)
  
    }
}
