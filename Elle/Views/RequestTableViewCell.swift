//
//  RequestTableViewCell.swift
//  Elle
//
//  Created by Andrea Garau on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit
import MapKit

/// Shows the help request with details such as last user position, name and description
class RequestTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var helpRequesterName: UILabel!
    @IBOutlet weak var helpDescription: UILabel!
    @IBOutlet weak var severityImageView: UIImageView!
    
    //MARK: - Properties
    var helpRequest: HelpRequestResponse?

    override func awakeFromNib() {
        super.awakeFromNib()
        mapView.delegate = self
        mapView.isUserInteractionEnabled = false
        mapView.layer.cornerRadius = 16
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// Set an help request and configure the cell based on its objects
    public func setHelpRequest(_ helpRequest: HelpRequestResponse, requesterName: String?) {
        self.helpRequest = helpRequest
        
        self.severityImageView.image = UIImage(systemName: "exclamationmark.triangle.fill")
        switch helpRequest.serverity {
        case 1:
            self.severityImageView.tintColor = .systemGreen
        case 2:
            self.severityImageView.tintColor = .systemYellow
        default:
            self.severityImageView.tintColor = .systemRed
        }
        
        // Set user location on Map
        let coordinates = CLLocationCoordinate2DMake(helpRequest.lat, helpRequest.lon)
        showLocation(coordinates)
        
        // Set user name
        if let requesterName = requesterName {
            self.helpRequesterName.text = requesterName != "" ? requesterName : NSLocalizedString("SCONOSCIUTO", comment: "Unknown")
        } else {
            self.helpRequesterName.text = NSLocalizedString("UTENTE", comment: "User")
        }
         
        self.helpDescription.text = helpRequest.description
    }
}

extension RequestTableViewCell: MKMapViewDelegate {
    private func showLocation(_ userCoordinates: CLLocationCoordinate2D) {
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = userCoordinates
        mapView.addAnnotation(dropPin)
        self.mapView.setRegion(MKCoordinateRegion(center: userCoordinates, span: span), animated: false)
    }
}
