//
//  CallButton.swift
//  Elle
//
//  Created by Andrea Garau on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import UIKit

/// Displays the name of the callee. Once pressed, it calls immediately.
class CallButton: UIButton {
    
    private let backgroundColorName = "callButtonBackgroundColor"
    
    public var calleeName: String = "" {
        didSet {
            let imageAttachment = NSTextAttachment()
            imageAttachment.image = UIImage(systemName: "phone.fill")
            
            let fullString = NSMutableAttributedString()
            fullString.append(NSAttributedString(attachment: imageAttachment))
            fullString.append(NSAttributedString(string: " \(self.calleeName)"))
            fullString.addAttribute(.foregroundColor, value: UIColor(red: 217.0, green: 255.0, blue: 248.0, alpha: 1.0), range: NSMakeRange(0, fullString.string.count))
            self.setAttributedTitle(fullString, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        layer.cornerRadius = 25
        backgroundColor = UIColor(named: backgroundColorName)
        titleLabel?.font = .systemFont(ofSize: 20.0, weight: .bold)
    }
    
}
