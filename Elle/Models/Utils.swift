//
//  Utils.swift
//  Elle
//
//  Created by Fabio Palladino on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation
import UIKit
public class Utils {
     
    public init() {
        
    }
    
    public static func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .short
        
        return dateFormatter.string(from: date)
    }
    
    public static func stringToDate(string: String) -> Date? {
        /*let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: string)*/
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.date(from: string)
    }
    public static func dicToJson(dic: [String:String]) -> String {
        let encode = JSONEncoder()
        var jsonValue: Data?
        do {
            encode.outputFormatting = .prettyPrinted
            jsonValue = try encode.encode(dic.self)
        }
        catch let error {
            print("toJson -> \(error)")
        }
        if jsonValue != nil {
            let jsonString = String(data: jsonValue!, encoding: .utf8)
            return jsonString ?? ""
        }
        return ""
    }
    public static func formatDate(string: String) -> String {
        if let d = stringToDate(string: string) {
            return dateToString(date: d)
        }
        return ""
    }
}
