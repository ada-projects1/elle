//
//  LoginInterfaceController.swift
//  Elle WatchKit Extension
//
//  Created by Gabriele Fioretti on 03/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import WatchKit
import Foundation


class LoginInterfaceController: WKInterfaceController {

    //MARK: - Outlets
    @IBOutlet weak var nameTextField: WKInterfaceTextField!
    @IBOutlet weak var phoneNumTextField: WKInterfaceTextField!
    @IBOutlet weak var tappo: WKInterfaceLabel!
    @IBOutlet weak var loginButton: WKInterfaceAuthorizationAppleIDButton!
    
    //MARK: - Variables
    var name: String = ""
    var phoneNum: String = ""
    
    //MARK: - View Lifecycle
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: - SignIn button action
    @IBAction func loginAction() {
        
    }
    
}
