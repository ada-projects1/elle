//
//  InterfaceController.swift
//  Elle WatchKit Extension
//
//  Created by Gabriele Fioretti on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import WatchKit
import Foundation
import UIKit
import AVFoundation

class InterfaceController: WKInterfaceController {
    
    //MARK: - Variables
    var progressTracker = 1
    var loadingTimer = Timer()
    let sharedInfo = ExtensionDelegate.shared.sharedInfo
    let delegate = ExtensionDelegate.shared
    var helpRequestManager: HelpRequestManager!
    var tokenDevice = UserDefaults.standard.string(forKey: "tokenDevice") ?? ""
    var isRequestActive: Bool = false

    
    //MARK: - Outlets
    @IBOutlet weak var supportButton: WKInterfaceButton!
    @IBOutlet weak var infoLabel: WKInterfaceLabel!
    @IBOutlet weak var buttonGroup: WKInterfaceGroup!
    
    //MARK: - Lifecycle
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        helpRequestManager = sharedInfo.helpRequestManager
        
        self.helpRequestManager.locationManager.requestWhenInUseAuthorization()
        
        //Setup initial view state
        if self.helpRequestManager.isActive {
            self.recordingAnimations()
        } else {
            self.stopAnimations()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
    }
    
    //MARK: - Button function
    @IBAction func sendSupportRequest() {
        if delegate.session.isReachable{
            delegate.session.sendMessage(["askState": true], replyHandler: { (reply) in
                if let iphoneState = reply["state"] as? Bool{
                    print("\n\nRecieved message: State = \(iphoneState)\n\n")
                    if !iphoneState{ //L'iphone ha una richiesta attiva
                        //MARK: Da vedere cosa fare
                        print("Iphone is active")
                        DispatchQueue.main.async {
                            let okAction = WKAlertAction(title: NSLocalizedString("ALERT_OK_ACTION", comment: "Ok"), style: .cancel) {}
                            self.presentAlert(withTitle: "Iphone is active", message: nil, preferredStyle: .alert, actions: [okAction])
                            return
                        }
                    } else {
                        self.startRecording()
                    }
                }
            }) { (error) in
                print(error.localizedDescription)
                //MARK: Scegliamo di mandare comunque la richiesta?
            }
        } else {
            startRecording()
        }
    }
    
    func updateColorOnSeverity() {
        //MARK: TODO
    }
    
    func startRecording() {
        if !sharedInfo.user.isLogged {
            return
        }
        
        if sharedInfo.user.friends.friends.isEmpty {
            let okAction = WKAlertAction(title: NSLocalizedString("ALERT_OK_ACTION", comment: "Ok"), style: .cancel) {}
            presentAlert(withTitle: NSLocalizedString("ALERT_NO_FRIENDS_TITLE", comment: ""), message: NSLocalizedString("ALERT_NO_FRIENDS_MESSAGE", comment: ""), preferredStyle: .actionSheet, actions: [okAction])
            return
        }
        
        do {
            /**
             * Questa parte si potrebbe riscrivere aggiungendo una funzione all'HelpRequestManager che medianta callback notifica al Controller se procedere o presentare l'alert
             */
            //Initialize AVSession to record
            helpRequestManager.avSession = AVAudioSession.sharedInstance()
            try helpRequestManager.avSession.setCategory(AVAudioSession.Category.playAndRecord)
            try helpRequestManager.avSession.setActive(true)
            
            //Initialize location manager (Check permissions)
            helpRequestManager.initializeLocationManager()
            
            //Check permissions
            helpRequestManager.avSession.requestRecordPermission() { granted in DispatchQueue.main.async {
                if !granted {
                    self.showNoPermissionsPopup()
                } else {
                    //MARK: Così facendo dovrebbe prendere lo stato direttamente dal server, così da capire se c'è o no una richiesta in corso
                    self.helpRequestManager.loadCurrentRequest()
                    if !self.helpRequestManager.isActive { //Nuova Help request
                        //let alert = self.displayNewRequestPendingAlert()
                        //Aggiorno status
                        self.delegate.currentStatus = .starting
                        self.helpRequestManager.startRecording { success in
                            if success {
                                DispatchQueue.main.async {
                                    //alert.dismiss(animated: true, completion: nil
                                    self.delegate.currentStatus = .started
                                    self.recordingAnimations()
                                    self.helpRequestManager.date = Date()
                                }
                            }
                        }
                    } else {//Aumentare severity
                        /*
                        self.helpRequestManager.updateSeverity { (success, errorMsg, response) in
                            if success {
                                self.updateColorOnSeverity()
                                print("Success")
                            } else {
                                print(errorMsg)
                            }
                        }
                         */
                        //Stop request
                        self.stopRequest()
                    }
                }
                }
            }
        } catch {
            // failed to record!
            print("Failed to record")
            helpRequestManager.finishRecording(success: false, isLastUpdate: false, callback: {_ in})
        }
    }
    
    //MARK: - Stop Function
    func stopRequest() {
        self.delegate.currentStatus = .finishing
        self.helpRequestManager.endRequest { success in
            DispatchQueue.main.async {
                self.delegate.currentStatus = .finished
                self.stopAnimations()
//                self.pulse?.isHidden = true
//                self.pulse1?.isHidden = true
            }
        }
    }
    
    //MARK: - Button animations
    func recordingAnimations(){
        infoLabel.setText(NSLocalizedString("INFOLABEL_RECORDING", comment: "Recording state is: \"Sending your request\""))
    }
    
    func stopAnimations() {
        //Stop animations
        infoLabel.setText(NSLocalizedString("INFOLABEL_NOT_RECORDING", comment: "Not recording state is: \"Tap to get help\""))
    }
    
    //MARK: - Display alert
    func showNoPermissionsPopup(){
        //Si dovrebbe fare un bottone per rimandare alle opzioni dell'app, ma essendo una companion app non so quanto è necessario
//                let action1 = WKAlertAction(title: NSLocalizedString("ALERT_SETTINGS_TITLE", comment: "Alert to go to settings"), style: .default, handler: {
//                    if UIApplication.shared.canOpenURL(settingsUrl) {
//                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                            print("Settings opened: \(success)")
//                        })
//                    }
//                })
        let action3 = WKAlertAction(title: NSLocalizedString("ALERT_CANCEL_ACTION", comment: "Cancel action"), style: .cancel) {}
        presentAlert(withTitle: NSLocalizedString("ALERT_SETTINGS_ACTION_TITLE", comment: "Go to settings of app"), message: "", preferredStyle: .actionSheet, actions: [/*action1,*/action3])
    }
    
    func showErrorPopup() {
        let action = WKAlertAction(title: NSLocalizedString("ALERT_CANCEL_ACTION", comment: "Cancel action"), style: .cancel) {}
        presentAlert(withTitle: NSLocalizedString("ALERT_AUDIOERROR_TITLE", comment: "Error in audio rec"), message: NSLocalizedString("ALERT_AUDIOERROR_MESSAGE", comment: "Error"), preferredStyle: .actionSheet, actions: [action])
    }
}


