//
//  WatchUtils.swift
//  Elle WatchKit Extension
//
//  Created by Gabriele Fioretti on 22/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation

public class WatchUtils {
    public static func dateToString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy - HH:mm"
        return formatter.string(from: date)
    }
}
