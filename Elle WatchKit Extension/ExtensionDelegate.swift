//
//  ExtensionDelegate.swift
//  Elle WatchKit Extension
//
//  Created by Gabriele Fioretti on 21/02/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import WatchKit
import WatchConnectivity

/**
 *Stando alla documentazione, questa classe è come l'AppDelegate dell'iphone
 */
class ExtensionDelegate: NSObject, WKExtensionDelegate {
    
    //MARK: - Soluzione: Usare una classe comune sia a watch che iOS
    let sharedInfo = SharedInfo.sharedInfo
    
    var user: UserApi
    var helpRequestManager: HelpRequestManager
    var session = WCSession.default
    var lastKnownIhponeStatus: RequestStatus? = nil
    var currentStatus: RequestStatus = .finished
    
    override init() {
        self.user = sharedInfo.user
        self.helpRequestManager = sharedInfo.helpRequestManager
        if !helpRequestManager.isActive {
            currentStatus = .finished
        } else {
            currentStatus = .started
        }
    }
    
    public static var API_URL = "https://elle.fabiopalladino.dev/"
    
    //MARK: - Variables
    public static var shared: ExtensionDelegate {
        get {
            return WKExtension.shared().delegate as! ExtensionDelegate
        }
    }
    
    var tokenDevice: String {
        get {
            return UserDefaults.standard.string(forKey: "tokenDevice") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "tokenDevice")
        }
    }

    //MARK: - Lifecycle
    func applicationDidFinishLaunching() {
        //Setup session between iPhone and watch
        session.delegate = self
        session.activate()
        //Do login to server
        login()
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    
    func applicationDidEnterBackground() {
        if helpRequestManager.isActive {
            helpRequestManager.endRequest(callback: {_ in})
        }
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
                // Be sure to complete the relevant-shortcut task once you're done.
                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
                // Be sure to complete the intent-did-run task once you're done.
                intentDidRunTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
    
    func login() {
        print("Token: ", self.tokenDevice)
        self.sharedInfo.user.loadUser(callback: { (success, error) in
            print("logged \(self.sharedInfo.user.isLogged)")
            self.helpRequestManager.loadCurrentRequest()
        })
    }
}

//MARK: Extension for watchConnectivity
extension ExtensionDelegate: WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        switch activationState {
        case .activated:
            debugPrint("Activated")
        default:
            debugPrint("Not activated")
        }
    }
    
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        DispatchQueue.main.async() {
            print("Recieved applicationContext")
            self.processApplicationContext()
        }
    }
        
    //To do something with recieved datas
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        print("Recieved userinfo")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("\n\nRecieved Message\n\n")
        if let recievedState = message["askState"]{
            if (recievedState as! Bool) == true {
                //Il watch vuole avviare una richiesta. Dobbiamo fargli sapere se può(true) o se ne sta facendo una il watch(false)
                if currentStatus == .finished {
                    replyHandler(["state": true])
                } else {
                    replyHandler(["state": false])
                }
            }
        }
    }
    
    func processApplicationContext() {
        let iPhoneContext = session.receivedApplicationContext
        if !iPhoneContext.isEmpty || UserDefaults.standard.string(forKey: "tokenDevice") == "" {
            let decoder = JSONDecoder()
            do {
                if let userInfo = iPhoneContext["currentUser"] {
                    self.sharedInfo.user = try decoder.decode(UserApi.self, from: (userInfo as! String).data(using: .utf8)!)
                    UserDefaults.standard.set(self.sharedInfo.user.token, forKey: "tokenDevice")
                    self.tokenDevice = sharedInfo.user.token
                    login()
                }
                if let status = iPhoneContext["iphoneStatus"] as? RequestStatus {
                    self.lastKnownIhponeStatus = status
                }
            } catch {
                print("Error in decoding or updating context")
            }
        }
    }
    
    /**
     * Probabilmente il modo migliore per mantenere uno stato tra i device è quello di salvarlo nel context dell'applicazione
     */
    func sendStatusToIphone(status: RequestStatus) {
        do {
            try session.updateApplicationContext(["watchStatus": status])
        } catch {
            print("Error in updating context!")
        }
    }
}
