//
//  SharedInfo.swift
//  Elle
//
//  Created by Gabriele Fioretti on 03/03/2020.
//  Copyright © 2020 Gabriele Fioretti. All rights reserved.
//

import Foundation

class SharedInfo: NSObject {
    
    //Variables
    public static let sharedInfo: SharedInfo = SharedInfo.init()                    //Permanente nell'applicazione
    var userData = UserDefaults.standard
    var user = UserApi()
    var helpRequestManager = HelpRequestManager()
    public static var API_URL = "https://elle.fabiopalladino.dev/"
    
    var tokenDevice: String {
        get {
            return UserDefaults.standard.string(forKey: "tokenDevice") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "tokenDevice")
        }
    }
}
